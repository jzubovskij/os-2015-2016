/* This module IS SMP safe */

/* Standard headers for LKMs */
#include <linux/kernel.h>
#include <linux/module.h>

/* We also need the ability to put ourselves to sleep
 * and wake up later */
#include <linux/sched.h>
#include <linux/delay.h>
#include <linux/kernel_stat.h>

/* This is used by cleanup, to prevent the module from
 * being unloaded while the kernel thread is still active */
static DECLARE_WAIT_QUEUE_HEAD( WaitQ);

static int worker_routine(void *);
static int please_clock_off = 0; /* signal to thread */

//define the cpu time after which a process would be inder consideration of being excessively buys
#define busyTime 1
//define the the number of cycles will an exscessively busy process be timed out for until being resumed
#define timeoutLimit 6
//define the maximum number of supported excessively busy tasks
#define supportedBusyTaskCount 1

//a marker indicating the cleanup is done
static int cleanupDone = 0;
//keep track of the pid of the excessively busy process
static int busyTaskPid = 0;
//keep track of the timeout cycles remaining
static int timeoutCycles = 0;
//keep track of if there are (is really as I support N=1) excessively busy processes
static int busyFound = 0;
//keepo track of keyboard interrupts
static int currentInterrupts = 0;

/* This is the routine that is run by the kernel thread we
 start when the modulue is loaded */
static int worker_routine(void *irrelevant) {
	/* drop userspace stuff */
	daemonize("stuckworker");
	printk("Your relaxer is clocking on\n");

	//set the keybpoard interrupts to the one before launching this module
	currentInterrupts = kstat_irqs(1);
	/* now do the work */
	while (1) {
		//keep track of a task in question
		struct task_struct *task;
		/* If cleanup wants us to die */
		if (please_clock_off) {
			printk("Your relaxer is clocking off\n");
			//set the flag that the acknowledgement to exit was received
			cleanupDone = 1;
			//alert the cleanup of aknowledgement recieval and that the cleanup can now exit
			wake_up(&WaitQ);
			//find the task identified by the recorded PID and before exiting the module, resume it
			task = find_task_by_pid_type(PIDTYPE_PID, busyTaskPid);
			//lock access to the critical section (task)
			task_lock(task);
			//send a wake-up (reusme) signal to the atsk
			kill_pid(task_pid(task), SIGCONT, 1);
			//print acknowledgement
			printk("RESUMING Command Name: [%s] PID: [%d]\n", task->comm,
					task->pid);
			//unlock access to the critical section (task)
			task_unlock(task);
			//exit the thread
			complete_and_exit(NULL, 0); /* terminate the kernel thread */
		} else {

			/* do some work */
			printk("Hello, this is your relaxer working hard\n");
			//read-copy-update synchronization lock (mutual exclusion for critical section)
			rcu_read_lock();
			
			//if there has been a keypress since the last time it was checked
			if (currentInterrupts != kstat_irqs(1)) {
				//keep count of how many excessively busy processes there are
				int busyTaskCount = 0;
				//check all tasks
				for_each_process(task)
				{
					//set up variables to hold the required data for each task
					struct timespec currentTimespec, startTimespec;
					int cpuTime = 0;
					int startTime = 0;
					int currentTime = 0;

					//get the CPU time
					cpuTime = cputime_to_secs(task->stime);
					//get the timespec for the start time
					startTimespec = task->start_time;
					//get the timespec for the current time
					do_posix_clock_monotonic_gettime(&currentTimespec);
					//get process start time in seconds
					startTime = startTimespec.tv_sec;
					//get current time in seconds
					currentTime = currentTimespec.tv_sec;
					//check if the process if busy (cpu time > defined cpu time maxymum and if 10% of the runtime is used on the CPU)
					if (cpuTime >= busyTime
							&& cpuTime * 10 >= (currentTime - startTime)) {
						//P.S. once again I only support one excessively busy process at a time, others are not handled, only reported (see next lines)
						//do not renew timer for currently monitored excessively busy tasks, only for new ones (if such number is supported)
						if (busyTaskCount < supportedBusyTaskCount && busyFound == 0) {
							//printing pausing acknowledgement
							printk("PAUSING Command Name: [%s] PID: [%d]\n",
									task->comm, task->pid);

							//lock access to the critical section (task)
							task_lock(task);

							//register taskfor timeout
							timeoutCycles = timeoutLimit;
							//record its PID
							busyTaskPid = task->pid;

							//send the task the signal to pause
							kill_pid(task_pid(task), SIGSTOP, 1);
							//unlock access to the critical section (task)
							task_unlock(task);
							//mark that there has been at least one excessively busy task
							busyFound = 1;
						}

						//increment the total excessively busy task count
						busyTaskCount++;

					}

				}
				//print acknowledgement that cannot support this many excessively busy tasks
				if (busyTaskCount > supportedBusyTaskCount) {
					printk(
							"WARNING: Busy task count <%i> exceeds supported count of <%i>",
							busyTaskCount, supportedBusyTaskCount);
				}
			}
			//putting this after the loop above makes sure that even if the user is continuosly typing, the excessively busy task will run for at least 1 cycle before being timed out again
			//timeout current excessibely busy task
			if (timeoutCycles == 0 && busyFound == 1) {
				//find the task identified by the recorded PID
				task = find_task_by_pid_type(PIDTYPE_PID, busyTaskPid);
				//lock access to the critical section (task)
				task_lock(task);
				//send a wake-up (reusme) signal to the atsk
				kill_pid(task_pid(task), SIGCONT, 1);
				//print acknowledgement
				printk("RESUMING Command Name: [%s] PID: [%d]\n", task->comm,
						task->pid);
				//unlock access to the critical section (task)
				task_unlock(task);
				//indicating that there is no longer something to resume
				busyFound = 0;

			}
			//if not time to resume the excessvely busy task
			else if (timeoutCycles > 0 && busyFound == 1) {
				//decrement the remaining cycle count
				timeoutCycles--;
			}
			//record new keyboard interrupt count
			currentInterrupts = kstat_irqs(1);

			//read-copy-update synchronization unlocked as critical section done
			rcu_read_unlock();
			/* schedule timeout will busy wait unless we say otherwise */
			/* sleep for 10 seconds */
			set_current_state(TASK_INTERRUPTIBLE);
			schedule_timeout(10 * HZ);
		}
	}
}

/* Initialize the module - start kernel thread */
int init_module() {
	kernel_thread(worker_routine, NULL, 0);
	return 0;
}

/* Cleanup */
void cleanup_module() {
	//alert the worker thread that it should be clocking off
	please_clock_off = 1;
	//also continues if event is already true (does not start the wait);
	wait_event_interruptible(WaitQ, cleanupDone == 1);
	//exit
}

MODULE_LICENSE("GPL");

