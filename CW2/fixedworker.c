
/* Standard headers for LKMs */
#include <linux/kernel.h>
#include <linux/module.h>  

/* We also need the ability to put ourselves to sleep 
 * and wake up later */
#include <linux/sched.h>
#include <linux/delay.h>

/* This is used by cleanup, to prevent the module from 
 * being unloaded while the kernel thread is still active */
static DECLARE_WAIT_QUEUE_HEAD(WaitQ);

static int worker_routine(void *);
static int please_clock_off = 0; /* signal to thread */

//a marker indicating the cleanup is done
static int cleanupDone = 0;


/* This is the routine that is run by the kernel thread we
   start when the modulue is loaded */
static int worker_routine(void *irrelevant)
{
  /* drop userspace stuff */
  daemonize("fixedworker");
  printk("Your FIXED worker is clocking on\n");
  /* now do the work */
  while ( 1 ) {
    /* If cleanup wants us to die */
    if (please_clock_off) {

      printk("Your FIXED worker is clocking off\n");
      //set the flag that the acknowledgement to exit was received
      cleanupDone = 1;
      //alert the cleanup of aknowledgement recieval and that the cleanup can now exit
      wake_up(&WaitQ);  
      //exit the thread
      complete_and_exit(NULL,0);  /* terminate the kernel thread */
    } else {
      /* do some work */
      printk("Hello, this is your FIXED worker working hard\n");
      /* schedule timeout will busy wait unless we say otherwise */
      /* sleep for 10 seconds */
      set_current_state(TASK_INTERRUPTIBLE);
      schedule_timeout(10*HZ);
    }
  }
}

/* Initialize the module - start kernel thread */
int init_module()
{
  kernel_thread(worker_routine,NULL,0);
  return 0;
}


/* Cleanup */
void cleanup_module()
{
  //alert the worker thread that it should be clocking off
  please_clock_off = 1;
  /* this 12 second sleep simulates the effect of having an SMP
     system, where the worker thread might be executing at the same
     time as rmmod */
  set_current_state(TASK_UNINTERRUPTIBLE);
  schedule_timeout(12*HZ);


  //also continues if event is already true (does not start the wait);
  wait_event_interruptible(WaitQ, cleanupDone == 1);	
  //exit
}
